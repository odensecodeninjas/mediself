﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Logging;

namespace Sygdom
{
    class User
    {
        Logger myLog = new Logger();
        List<User> userList = new List<User>();

        public int UserId { get; set; }
        public string CPRNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string UserAddress { get; set; }
        public string Password { get; set; }
        public string CurrentUser { get; set; }
        public int CurrentUserId { get; set; }

        public bool userOnline = false;

        public User (string data)
        {
            string[] array = data.Split(';');
            this.UserId = int.Parse(array[0]);
            this.CPRNumber = array[1];
            this.PhoneNumber = array[2];
            this.UserAddress = array[3];
            this.Password = array[4];         
        }
        public User()
        {
            this.UserId = GetId();
            this.CPRNumber = "99";
            this.PhoneNumber = "99";
            this.UserAddress = "99";
            this.Password = "100";
        }


        public override string ToString()
        {
            return UserId + "; " + CPRNumber + "; " + PhoneNumber +";" + UserAddress + ";" + Password + ";";
        }
        public void UserLogIn()
        {
            List<User> FindCPR = new List<User>();
            foreach (var line in File.ReadAllLines(@"..\Users.txt"))
            {
                User user = new User(line);
                FindCPR.Add(user);
            }
            Console.WriteLine("What is your CPR number?");
            string userInput = Console.ReadLine();

            myLog.FilePath(@"../");
            myLog.FileName("user_login.txt");                       

            foreach (var item in FindCPR)
            {
                if (item.CPRNumber == userInput)
                {
                    Console.WriteLine("What is your password?");
                    string inputPassword = Console.ReadLine();
                    if (item.Password == inputPassword)
                    {
                        myLog.Text("User successfully logged in with CPR: " + userInput);
                        CurrentUser = item.CPRNumber;
                        CurrentUserId = item.UserId;
                        userOnline = true;
                        Console.Clear();
                        break;
                    }else
                    {
                        myLog.Text("User had wrong password CPR: " + userInput);
                        Console.WriteLine("Your password didn't match your CPR number");
                    }
                } else
                {
                    myLog.Text("User failed to login with CPR: " + userInput);                    
                }                
            }
            if(userOnline == false)
            {
                Console.WriteLine("We don't have a user with that CPR number");
            }
            
            myLog.Close();
        }
        public void RegisterUser()
        {
            User user = new User();
            user.UserId = GetId();
            Console.WriteLine("What is your CPR number?");
            user.CPRNumber = Console.ReadLine();
            Console.WriteLine("What is your phone number?");
            user.PhoneNumber = Console.ReadLine();
            Console.WriteLine("What is your address");
            user.UserAddress = Console.ReadLine();
            user.Password = GetPassword();
            
            userList.Add(user);

            using (StreamWriter Writer = File.AppendText(@"..\Users.txt"))
            {
                myLog.FilePath(@"../");
                myLog.FileName("user_register.txt");
                myLog.Text("User successfully registered: " + user.ToString());
                myLog.Close();
                Writer.WriteLine(user.ToString());
                Writer.Dispose();
            }
            
        }
        public int GetId()
        {
            int idOutput = 0;
            string idSubstring = "";
            string test = File.ReadLines(@"..\Users.txt").LastOrDefault();
          
            if (test != "")
            {
                idSubstring = File.ReadLines(@"..\Users.txt").Last();
                int index = idSubstring.IndexOf(';');
                idOutput = int.Parse(idSubstring.Substring(0, index));
            } 
            else
            {
                Console.WriteLine("Something went wrong, contact system administrator.");
            }     
            
            return idOutput + 1;
        }
        public void ChangeUserInfo()
        {
            
        }
        public string GetPassword()
        {
            Console.WriteLine("What do you want your password to be? - be aware of capital letters");
            string password = Console.ReadLine();
            Console.WriteLine("Please repeat your password");
            if (Console.ReadLine() == password)
            {
                Console.WriteLine("Thank you, you've now been added as a user");
            } else
            {
                Console.WriteLine("Your passwords didn't match, please try again");
                GetPassword();
            }

            return password;
        }
    }
}
