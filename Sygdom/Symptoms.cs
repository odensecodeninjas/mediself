﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Sygdom
{

    class Symptoms
    {

        static public List<Symptoms> symptons = new List<Symptoms>();

        public int symptomId { get; set; }
        private string symptomName { get; set; }
        public int symptomWarningPoints { get; set; }
        public string symptomDescription { get; set; }

        public Symptoms(string allData)
        {
            string[] myArray = allData.Split(';');
            this.symptomId = int.Parse(myArray[0]);
            this.symptomName = myArray[1];
            this.symptomWarningPoints = int.Parse(myArray[2]);
            this.symptomDescription = myArray[3];
        }

        public Symptoms()
        {
            this.symptomId = symptomId;
            this.symptomName = symptomName;
            this.symptomWarningPoints = symptomWarningPoints;
            this.symptomDescription = symptomDescription;
        }

        public void AddSymptom()
        {

        }

        static public List<Symptoms> SearchSymptomByName(string searchString)
        {
            string[] myArray = searchString.Split(',');
            
            string pattern = "";

            List<int> symps = new List<int>();
            List<string> allowedWords = new List<string>();

            List<Symptoms> test = new List<Symptoms>();
            List<Symptoms> tester = new List<Symptoms>();

            string pathProd = @"..\symptomer.txt";

            foreach (var symptom in File.ReadAllLines(pathProd))
            {
                Symptoms symptonList = new Symptoms(symptom);
                symptons.Add(symptonList);
            }

            for (int i = 0; i < myArray.Length; i++)
            {
                pattern = myArray[i];
                bool stringHasSpace = pattern.Contains(" ");

                if (stringHasSpace)
                {
                    string[] arrayOfWords = myArray[i].Split(' ');
                    for (int y = 0; y < arrayOfWords.Length; y++)
                    {
                        if(arrayOfWords[y].Length > 3) {
                            allowedWords.Add(arrayOfWords[y]);                            
                        }
                    }

                    foreach (var item in allowedWords)
                    {
                        string newItem = item.Trim().ToLower();

                        bool exists = symptons.Exists(x => x.symptomName.Contains(newItem));

                        if (exists)
                        {
                            tester = symptons.FindAll(x => x.symptomName.Contains(newItem));
                            foreach (var symptom in tester)
                            {
                                test.Add(symptom);
                            }
                        }
                    }
                }
                else
                {
                    string newItem = pattern.Trim().ToLower();
                    bool exists = symptons.Exists(x => x.symptomName.Contains(newItem));

                    if (exists)
                    {
                        tester = symptons.FindAll(x => x.symptomName.Contains(newItem));
                        foreach (var symptom in tester)
                            {
                                test.Add(symptom);
                            }
                        }
                    }
                }

            return test;
        }          

        static public List<Symptoms> GetAllSymptoms()
        {
            string pathProd = @"..\symptomer.txt";
            foreach (var symptom in File.ReadAllLines(pathProd))
            {
                Symptoms symp = new Symptoms(symptom);
                symptons.Add(symp);
            }

            return symptons;
        }

        public override string ToString()
        {
            return "Symptom ID: " + symptomId + "\nSymptoms: " + symptomName + "\nDescription: " + symptomDescription+ "\nWarning Points: " + symptomWarningPoints+"\n";
        }

    }
}
