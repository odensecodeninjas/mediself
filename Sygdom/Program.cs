﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Logging;
using System.Text.RegularExpressions;

namespace Sygdom
{
    public class Program
    {
        Logger myLog = new Logger();
        User myUser = new User();        
        static void Main(string[] args)
        {
            Program myProgram = new Program();
            myProgram.Run();             
        }
        private void Run()
        {
            int choice;
            bool keepGoing = true;

            while (keepGoing)
            {
                choice = ShowMenu();
                switch (choice)
                {
                    case 1:
                        ContactDoctor();
                        break;
                    case 2:
                        SelfHelp();
                        break;
                    case 3:
                        myUser.ChangeUserInfo();
                        break;
                    case 4:
                        Prescription();
                        break;
                    case 8:
                        Console.Clear();
                        myUser.userOnline = false;
                        Console.WriteLine("You have been logged out! \n");
                        break;
                    case 9:
                        myUser.RegisterUser();
                        break;
                    case 10:
                        Exit();
                        keepGoing = false;
                        break;
                    default:
                        break;
                }
            }
        }
        public int ShowMenu()
        {
            if (myUser.userOnline)

            {
                int input = 0;
                Console.WriteLine("Hello " + myUser.CurrentUser + "\nWhat can we do for you today?\n");
                Console.WriteLine("1) Contact Doctor.");
                Console.WriteLine("2) Self Diagnose.");
                Console.WriteLine("3) Change personal info.");
                Console.WriteLine("4) Prescriptions.");
                Console.WriteLine("8) Log out.");
                Console.WriteLine("9) Register new user.");
                Console.WriteLine("10) Exit \n\n");
                try
                {
                    input = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    ErrorPrinter(01);
                }
                return input;
            } else {
<<<<<<< HEAD
                Console.WriteLine("Please log in");
=======
                Console.WriteLine("Please log in now!");
>>>>>>> 5b5aa044589f7488332f949f6bc7c35b79e03d5b
                myUser.UserLogIn();
                return 0;
            }
        }
        public void Prescription()
        {
            Prescription test = new Prescription();
            string prescriptionChoice = "0";
            Console.WriteLine("Hello " + myUser.CurrentUser + "\nPlease make a choice:\n");
            Console.WriteLine("1) Print my prescriptions");
            Console.WriteLine("2) Add new prescription");
            
            try
            {
                prescriptionChoice = Console.ReadLine();

                if (prescriptionChoice == "1")
                {
                    Console.Clear();
                    test.myPrescriptions(myUser.CurrentUserId);

                } else if (prescriptionChoice == "2")
                {
                    Console.Clear();
                    test.addPrescription();
                }
                else
                {
                    ErrorPrinter(05);
                }
            }
            catch (FormatException)
            {
                ErrorPrinter(01);
            }
            
        }
        private void Exit()
        {
            Console.WriteLine("\nThank you, com' again!\n");
            System.Threading.Thread.Sleep(2000);
        }
        private void ContactDoctor()
        {
            string phoneNumber = "12345678";
            Console.WriteLine("Is your current phone number {0}?",phoneNumber);
            Console.WriteLine("Yes? \nNo?");
            if (Console.ReadLine() == "No" || Console.ReadLine() == "no")
            {
                Console.WriteLine("Alright, type in your current phone number and a doctor will contact you ASAP");
                phoneNumber = Console.ReadLine();
            } else if (Console.ReadLine() == "Yes" || Console.ReadLine() == "yes")
            {
                Console.WriteLine("Alright, a doctor will contact you on that number ASAP");
            } else
            {
                Console.WriteLine("You need to write 'Yes' or 'No'");
            }
        }
        private void SelfHelp()
        {

            List<int> list = new List<int>();
            int input = 0;
            int amountOfMatches = 0;
            Console.Clear();
            Console.WriteLine("Skriv dine symptom(er)! Del med komma, hvis du har flere symptomer!");

            string symp = Console.ReadLine();

            myLog.FilePath(@"../");
            myLog.FileName("symptom_input.txt");
            myLog.Text("User had these symptoms: " + symp);
            myLog.Close();

            List<Symptoms> symptomList = Symptoms.SearchSymptomByName(symp);
            List<Symptoms> distinct = symptomList.Distinct().ToList();
            List<Illness> illnessList = Illness.GetAllIllnesses();

            List<Details> details = new List<Details>();

            foreach (var item in distinct)
            {
                Console.WriteLine(item.symptomId);
                int symptom = item.symptomId;
            }

            //foreach (var illItem in illnessList)
            //{
            //    List<Illness> item = illItem.FindMatchingSymptoms(distinct);
            //    //Console.WriteLine(item);
            //}


            if (distinct.Count != 0)
            {
                foreach (var item in distinct)
                {
                    list.Add(item.symptomId);

                    //Details info = new Details(list, illnessList);
                    foreach (var illItem in illnessList)
                    {
                        amountOfMatches = Regex.Matches(illItem.symptomList, @"(^|\s)" + Regex.Escape(item.symptomId.ToString()) + "(?=,|$)").Count;

                        if (amountOfMatches != 0)
                        {
                            Console.WriteLine(illItem.ToString());
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Ingen sygdomme der matcher de symptomer!");
            }

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey(true);
        }

        private void ErrorPrinter(int errorValue)
        {
            string printError = "";

            if (errorValue == 01)
            {
                printError = " - - - - - - - - - \n - Error: Please pick a number -\n - - - - - - - - - - - -";
            }
            else if (errorValue == 02)
            {
                printError = "- - - - - - - - - -\n- Error: Not a valid option  -\n- - - - - - - - - -";
            }
            else if (errorValue == 03)
            {
                printError = "- - - - - - - - - -\n- Error: Not a valid option  -\n- - - - - - - - - -";
            }
            else if (errorValue == 04)
            {
                printError = "- - - - - - - - - -\n- Error: Not a valid option  -\n- - - - - - - - - -";
            }
            else if (errorValue == 05)
            {

                printError = "- - - - - - - - - -\n- Error: Choice does not exist  -\n- - - - - - - - - -";
            }
            else
            {
                printError = " - - - - - - - - - -\n - Error: Unindentified -\n - - - - - - - - - -";
            }
            Console.WriteLine(printError);
        }

    }
}