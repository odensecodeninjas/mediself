﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Logging;

namespace Sygdom
{
    class Prescription
    {
        public string prescriptionName { get; set; }
        public string prescriptionInfo { get; set; }
        public string prescriptionEndDate { get; set; }
        public string prescriptionId { get; set; }
        public string prescriptionUserId { get; set; }
        public string prescriptionCpr { get; set; }
        public string prescriptionTlf { get; set; }
        public string prescriptionAddr { get; set; }
        public string prescriptionPw { get; set; }

        public void myPrescriptions(int input)
        {          
            List<string> listPrescription = new List<string>();

            Console.WriteLine("\n\nYour Prescriotions:");

            foreach (var line in File.ReadLines(@"..\Prescription.txt"))
            {
                int leBreak = line.IndexOf(";");
                
                if (input == Convert.ToInt32(line.Substring(0, leBreak)))
                {

                    string[] array = line.Split(';');
                    this.prescriptionId = array[0];
                    this.prescriptionName = array[1];
                    this.prescriptionInfo = array[2];
                    this.prescriptionEndDate = array[3];

                    Console.WriteLine("Prescription name:" + prescriptionName);
                    Console.WriteLine("Prescription informtion:" + prescriptionInfo);
                    Console.WriteLine("Prescription valid until:" + prescriptionEndDate+"\n\n");
                }
                else
                {
                }
            }
        }
        public void addPrescription()
        {
            foreach (var line in File.ReadLines(@"..\Users.txt"))
            {
                string[] array = line.Split(';');
                this.prescriptionUserId = array[0];
                this.prescriptionCpr = array[1];
                this.prescriptionTlf = array[2];
                this.prescriptionAddr = array[3];
                this.prescriptionPw = array[4];

                Console.WriteLine(prescriptionUserId+": "+prescriptionCpr);

            }
            Console.WriteLine("Add Prescriotion User:");
            string prescriptionUser = Console.ReadLine();

            Console.Clear();

            Console.WriteLine("Add Prescription Name:");
            string prescriptionName = Console.ReadLine();

            Console.WriteLine("Add Prescription Information:");
            string prescriptionInfo = Console.ReadLine();

            Console.WriteLine("Add Prescription End Date:");
            string prescriptionEndDate = Console.ReadLine();

            string newPrescription = prescriptionUser + ";" +prescriptionName + "," + prescriptionInfo + ";" + prescriptionEndDate;
            
            using (StreamWriter Writer = File.AppendText(@"..\Prescription.txt"))
            {
                Writer.WriteLine(newPrescription.ToString());
                Writer.Dispose();
            }
        }

    }
}
